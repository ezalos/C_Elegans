import argparse
from config import c_datasets
import cv2


def load_video(path, print_frame=False):
	cap = cv2.VideoCapture(path)
	while not cap.isOpened():
		cap = cv2.VideoCapture(path)
		cv2.waitKey(1000)
		print("Wait for the header")

	# ? Before was:
	# 	pos_frame = cap.get(cv2.cv.CV_CAP_PROP_POS_FRAMES)
	pos_frame = cap.get(1)
	while True:
		flag, frame = cap.read()
		if flag:
			# * The frame is ready and already captured
			cv2.imshow('video', frame)
			# ? Before was:
			# 	pos_frame = cap.get(cv2.cv.CV_CAP_PROP_POS_FRAMES)
			pos_frame = cap.get(1)
			print(str(pos_frame)+" frames")
			if print_frame:
				print(frame)
		else:
			# * The next frame is not ready, so we try to read it again
			cap.set(cv2.cv.CV_CAP_PROP_POS_FRAMES, pos_frame-1)
			print("frame is not ready")
			# * It is better to wait for a while for the next frame to be ready
			cv2.waitKey(1000)

		if cv2.waitKey(10) == 27:
			break
		# ! ERROR: 
		#		AttributeError: cv2.cv.CV_CAP_PROP_FRAME_COUNT
		#			-> Is the next if really useful ?
		# if cap.get(cv2.cv.CV_CAP_PROP_POS_FRAMES) == cap.get(cv2.cv.CV_CAP_PROP_FRAME_COUNT):
		# 	# * If the number of captured frames is equal to the total number of frames,
		# 	# * we stop
		# 	break


if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument(
		"-f", "--file", help="File to load")
	args = parser.parse_args()
	if args.file:
		load_video(c_datasets.loaction + args.file, print_frame=False)
