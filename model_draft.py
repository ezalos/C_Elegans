import numpy as np
from sklearn.model_selection import train_test_split


# 1. Collecte data 

# 2. Preprocessing ?

# 3. Train-test split

# 4. Train-dev split

# 5. Train model over training set

# 6. Eval model performances over dev set
# 6.1 Error evaluation 
# 6.2 Fix the most important source of errors
# -> Train again 